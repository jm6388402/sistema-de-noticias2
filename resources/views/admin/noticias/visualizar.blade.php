@extends('layouts.admin')


@section('titulo','Área administrativa')
    
@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Visualizar Notícia</h2>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
                <table class="table table-striped table-condensed">
                    <tr>
                        <th width="50">ID</th>
                        <td>1</td>
                    </tr>
                    <tr>
                        <th width="50">Titulo</th>
                        <td>Flamengo ganhou do Corinthians</td>
                    </tr>
                    <tr>
                        <th width="50">SubTitulo</th>
                        <td>Tem jogo de volta</td>
                    </tr>
                    <tr>
                        <th width="50">Descrição</th>
                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad, sequi. Repudiandae quisquam doloribus ut natus officiis qui numquam, repellat earum, eligendi laboriosam, quod autem optio? Cupiditate necessitatibus a eligendi itaque?</td>
                    </tr>
                    <tr>
                        <th width="50">Status</th>
                        <td>Não publicado</td>
                    </tr>
                </table>    
                    <a href="#" class="btn btn-danger">Editar Notícia</a>
                    <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>
</div>

@endsection