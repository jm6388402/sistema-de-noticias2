<form action="" method="post">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="titulo">Título</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" id="titulo" name="titulo" value="">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">SubTítulo</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" id="subtitulo" name="subtitulo" value="">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Descrição</label>
        <div class="col-sm-10">
            <textarea class="form-control" id="descricao" name="descricao" value=""></textarea>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Descrição</label>
        <div class="col-sm-5">
            <select name="status" id="status" class="form-control">
                <option value="0">Não Publicado</option>
                <option value="1">Aguardando Revisão</option>
                <option value="2">Publicado</option>
            </select>
        </div>
    </div>

    <div class="for-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Enviar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>

    
</form>