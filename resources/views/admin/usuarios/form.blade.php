<form action="" method="post">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="nome">Nome</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" id="nome" name="nome" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="email">E-mail</label>
        <div class="col-sm-10">
            <input class="form-control" type="text" id="email" name="email" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="senha">Password</label>
        <div class="col-sm-2">
            <input class="form-control" type="text" id="senha" name="senha" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="perfil">Perfil</label>
        <div class="col-sm-5">
            <input class="form-control" type="text" id="perfil" name="perfil" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="foto">Foto</label>
        <div class="col-sm-5">
            <img class="img-fluid" src="https://via.placeholder.com/200x200">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Permissão</label>
        <div class="col-sm-5">
            <select name="status" id="status" class="form-control">
                <option value="0">Colaborador</option>
                <option value="1">Editor</option>
                <option value="2">Adiministrador</option>
            </select>
        </div>
    </div>
</form>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Status</label>
        <div class="col-sm-5">
            <select name="status" id="status" class="form-control">
                <option value="0">Ativo</option>
                <option value="1">Inativo</option>
            </select>
        </div>
    </div>

    <div class="for-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-danger">Salvar</button>
            <a href="#" class="btn btn-secondary">Cancelar</a>
        </div>
    </div>
</form>