<?php
//Seção Home
Route::get('/', function () {
    return view('home.index');
});
//Seção Notícia
Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});

Route::get('/contato', function () {
    return view('home.contato');
});

//Seção Admin
Route::get('/admin/home', function () {
    return view('admin.home.index');
});

Route::get('/admin/noticias', function () {
    return view('admin.noticias.index');
});

Route::get('/admin/noticias/cadastrar', function () {
    return view('admin.noticias.cadastrar');
});
Route::get('/admin/noticias/editar', function () {
    return view('admin.noticias.editar');
});
Route::get('/admin/noticias/visualizar', function () {
    return view('admin.noticias.visualizar');
});

Route::get('/admin/categoria/index', function () {
    return view('admin.categoria.index');
});

Route::get('/admin/usuarios/index', function () {
    return view('admin.usuarios.index');
});

Route::get('/admin/usuarios/editar', function () {
    return view('admin.usuarios.editar');
});



Route::get('/admin/categoria/index', function () {
    return view('admin.categoria.index');
});

Route::get('/admin/categoria/editar', function () {
    return view('admin.categoria.editar');
});


